import React from 'react';
import {ConnectedRouter} from 'connected-react-router';
import {History} from 'history';
import {Connect} from '../../common/decorators/connect.decorator';
import {initApp} from '../store/app.actions';
import routes from '../routes/app.routes';
import {IAppState} from "../store/app.state";

interface IProps {
    history: History;
    initApp?: typeof initApp;
    app?: IAppState;
}

@Connect(({app}) => ({
    app: app,
}), {
    initApp: initApp,
})
export default class App extends React.Component<IProps> {
    public componentDidMount(): void {
        this.props.initApp!();
    }

    public render(): React.ReactNode {
        return (
            this.props.app!.isLoading
                ? 'loading'
                : <ConnectedRouter history={this.props.history}>{routes}</ConnectedRouter>
        )
    }
}

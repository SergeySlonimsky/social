import React from 'react';
import {Switch} from 'react-router';
import PublicRoute from "../../common/components/router/public-route.component";
import PrivateRoute from "../../common/components/router/private-route.component";
import Auth from "../../auth/containers/auth.container";
import Home from "../../main/home/containers/home.container";

const routes = (
    <Switch>
        <PublicRoute path="/auth" component={Auth}/>
        <PrivateRoute path="/" component={Home}/>
    </Switch>
);

export default routes

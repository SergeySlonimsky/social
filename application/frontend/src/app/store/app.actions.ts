import {
    generateBaseActionCreator,
    generatePayloadActionCreator,
    TBaseActionCreator,
    TPayloadActionCreator,
} from '../../common/state/generic.actions';

export const initApp: TBaseActionCreator =
    generateBaseActionCreator('app.init');

export const setAppLoading: TPayloadActionCreator<boolean> =
    generatePayloadActionCreator('app.loading');

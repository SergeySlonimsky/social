import {setAppLoading} from './app.actions';
import {combineReducers} from "redux";
import {generatePrimitiveReducer} from "../../common/state/generic.reducers";
import {initialAppState} from "./app.state";

export const appReducer = combineReducers({
    isLoading: generatePrimitiveReducer<boolean>(setAppLoading.type, initialAppState.isLoading),
});

export default appReducer

import {SagaIterator} from 'redux-saga';
import {put, takeLatest} from 'redux-saga/effects';
import {initApp, setAppLoading} from "./app.actions";
import {getToken} from "../../common/utils/tokenService";
import {isAuth} from "../../auth/store/auth.actions";
import {genericErrorHandler} from "../../error/state/error.saga";
import {setError} from "../../error/state/error.action";
import {LOCATION_CHANGE} from "connected-react-router";

function* onInitApp(): SagaIterator {
    if (getToken()) {
        yield put(isAuth(true));
    }
    yield put(setAppLoading(false));
}

function* onLocationChange():SagaIterator {
    yield put(setError(null));
}

export function* appSaga(): SagaIterator {
    yield takeLatest(initApp.type, genericErrorHandler(onInitApp));
    yield takeLatest(LOCATION_CHANGE, genericErrorHandler(onLocationChange));
}

export interface IAppState {
    isLoading: boolean;
}

export const initialAppState: IAppState = {
    isLoading: true
};

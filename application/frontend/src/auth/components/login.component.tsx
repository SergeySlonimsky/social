import React from 'react';
import {Connect} from "../../common/decorators/connect.decorator";
import {IAuthState} from "../store/auth.state";
import {Field, FieldProps, Form, Formik} from "formik";
import {login} from "../store/auth.actions";
import {ILoginCredentials} from "../models/credentials.model";
import SclFormInput from "../../common/components/form/scl-form-input.component";
import SclFormSubmit from "../../common/components/form/scl-form-submit.component";

interface IProps {
    auth: IAuthState;
    login: typeof login;
}

@Connect(({auth}) => ({auth}), {login: login})
export default class Login extends React.Component<IProps> {
    private initialValues: ILoginCredentials = {
        username: '',
        password: '',
    };

    public render(): React.ReactNode {
        return (
            <div className={'col-md-4 d-flex justify-content-center'}>
                <Formik
                    initialValues={this.initialValues}
                    onSubmit={this.onSubmit}
                    render={this.renderForm}
                />
            </div>
        )
    }

    private renderForm = () => {
        return (
            <Form className={'auth-form'}>
                <Field
                    name="username"
                    render={({field}: FieldProps<ILoginCredentials>) => (
                        <SclFormInput name={'username'} label={'Username'} {...field} />
                    )}
                />
                <Field
                    name="password"
                    render={({field}: FieldProps<ILoginCredentials>) => (
                        <SclFormInput name={'password'} label={'Password'} type={'password'} {...field} />
                    )}
                />
                <SclFormSubmit isFormSending={this.props.auth.isLoading} />
            </Form>
        )
    }

    private onSubmit = (values: ILoginCredentials) => {
        this.props.login(values);
    }
}

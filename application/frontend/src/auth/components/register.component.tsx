import React from 'react';
import {Form, Field, Formik, FieldProps} from "formik";
import {Connect} from "../../common/decorators/connect.decorator";
import {IAuthState} from "../store/auth.state";
import {IRegisterCredentials} from "../models/credentials.model";
import {register} from "../store/auth.actions";

interface IProps {
    auth: IAuthState;
    register: typeof register;
}

@Connect(({auth}) => ({auth}), {register: register})
export default class Register extends React.Component<IProps> {
    private initialValues: IRegisterCredentials = {
        username: '',
        email: '',
        password: '',
    };

    public render(): React.ReactNode {
        return (
            <div className={'col-md-4 d-flex justify-content-center'}>
                <Formik
                    initialValues={this.initialValues}
                    onSubmit={this.onSubmit}
                    render={this.renderForm}
                />
            </div>
        )
    }

    private renderForm = () => {
        return (
            <Form className={'auth-form'}>
                <Field
                    name="username"
                    render={({ field }: FieldProps<IRegisterCredentials>) => (
                        <div className="form-group">
                            <input
                                disabled={this.props.auth.isLoading}
                                type="text"
                                {...field}
                            />
                        </div>
                    )}
                />
                <Field
                    name="email"
                    render={({ field }: FieldProps<IRegisterCredentials>) => (
                        <div className="form-group">
                            <input
                                disabled={this.props.auth.isLoading}
                                type="text"
                                {...field}
                            />
                        </div>
                    )}
                />
                <Field
                    name="password"
                    render={({ field }: FieldProps<IRegisterCredentials>) => (
                        <div className="form-group">
                            <input
                                disabled={this.props.auth.isLoading}
                                type="text"
                                {...field}
                            />
                        </div>
                    )}
                />
                <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={this.props.auth.isLoading}
                >
                    {this.props.auth.isLoading ? 'loading' : 'Submit'}
                </button>
            </Form>
        )
    }

    private onSubmit = (values: IRegisterCredentials) => {
        this.props.register(values);
    }
}

import React from 'react';
import {Connect} from "../../common/decorators/connect.decorator";
import {IErrorState} from "../../error/state/error.state";
import ResponseError from "../../error/components/response-error.component";
import routes from "../routes/auth.routes";
import {Link} from "react-router-dom";

interface IProps {
    error: IErrorState;
}

@Connect(({error}) => ({error: error}))
export default class Auth extends React.Component<IProps> {
    public render(): React.ReactNode {
        return (
            <div className={'auth-container'}>
                <ul>
                    <li><Link to={'/auth'}>Login</Link></li>
                    <li><Link to={'/auth/register'}>Register</Link></li>
                </ul>
                <div>
                    {this.props.error.error && <ResponseError error={this.props.error.error} />}
                    <img src={'/img/logo.png'} height={150} alt={'auth-logo'}/>
                    {routes}
                </div>
            </div>
        )
    }
}

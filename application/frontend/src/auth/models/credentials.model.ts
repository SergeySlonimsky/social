export interface IRegisterCredentials {
    email: string;
    username: string;
    password: string;
}

export interface ILoginCredentials {
    username: string;
    password: string;
}

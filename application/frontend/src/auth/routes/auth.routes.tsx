import React from 'react';
import {Route, Switch} from 'react-router';
import Login from "../components/login.component";
import Register from "../components/register.component";

const routes = (
    <Switch>
        <Route exact path="/auth" component={Login}/>
        <Route exact path="/auth/register" component={Register}/>
    </Switch>
);

export default routes

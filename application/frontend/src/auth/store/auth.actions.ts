import {ILoginCredentials, IRegisterCredentials} from '../models/credentials.model';
import {
    generateBaseActionCreator,
    generatePayloadActionCreator,
    TBaseActionCreator,
    TPayloadActionCreator
} from '../../common/state/generic.actions';

export const login: TPayloadActionCreator<ILoginCredentials> =
    generatePayloadActionCreator('auth.login');

export const logout: TBaseActionCreator =
    generateBaseActionCreator('auth.logout');

export const register: TPayloadActionCreator<IRegisterCredentials> =
    generatePayloadActionCreator('auth.register');

export const isAuth: TPayloadActionCreator<boolean> =
    generatePayloadActionCreator('auth.isAuth');

export const isLoading: TPayloadActionCreator<boolean> =
    generatePayloadActionCreator('auth.isLoading');

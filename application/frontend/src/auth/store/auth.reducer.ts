import {combineReducers} from "redux";
import {generatePrimitiveReducer} from "../../common/state/generic.reducers";
import {isAuth, isLoading} from "./auth.actions";
import {initialAuthState} from "./auth.state";

export const authReducer = combineReducers({
    isAuth: generatePrimitiveReducer<boolean>(isAuth.type, initialAuthState.isAuth),
    isLoading: generatePrimitiveReducer<boolean>(isLoading.type, initialAuthState.isLoading),
});

export default authReducer

import {SagaIterator} from 'redux-saga';
import {call, put, takeLatest} from 'redux-saga/effects';
import AuthApi from '../utils/auth.api';
import {TPayloadAction} from '../../common/models/action.model';
import {ILoginCredentials, IRegisterCredentials} from '../models/credentials.model';
import {IResponse} from '../../common/models/response.model';
import {isAuth, isLoading, login, logout, register} from './auth.actions';
import {removeToken, setToken} from '../../common/utils/tokenService';
import {setAppLoading} from "../../app/store/app.actions";
import {setProfile} from "../../user/store/user.actions";
import {genericErrorHandler} from "../../error/state/error.saga";

function* onRegister(action: TPayloadAction<IRegisterCredentials>): SagaIterator {
    yield call(AuthApi.register, action.payload);
}

function* onLogin(action: TPayloadAction<ILoginCredentials>): SagaIterator {
    yield put(isLoading(true));
    const response: IResponse<{ token: string }> = yield call(AuthApi.login, action.payload);
    setToken(response.data.data.token);
    yield put(isAuth(true));
    yield put(isLoading(false));
}

function* onLogout(): SagaIterator {
    removeToken();
    yield put(setProfile(null));
    yield put(isAuth(false));
    yield put(setAppLoading(false));
}

export function* authSaga(): SagaIterator {
    yield takeLatest(register.type, genericErrorHandler(onRegister));
    yield takeLatest(login.type, genericErrorHandler(onLogin));
    yield takeLatest(logout.type, genericErrorHandler(onLogout));
}

export interface IAuthState {
    isAuth: boolean;
    isLoading: boolean;
}

export const initialAuthState: IAuthState = {
    isAuth: false,
    isLoading: false,
};

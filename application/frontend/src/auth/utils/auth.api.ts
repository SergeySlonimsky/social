import BaseApi from "../../common/utils/base.api";
import {ILoginCredentials, IRegisterCredentials} from "../models/credentials.model";

export default class AuthApi {
    public static register = (request: IRegisterCredentials) => {
        return BaseApi.doPost('auth/register', request);
    }

    public static login = (request: ILoginCredentials) => {
        return BaseApi.doPost('auth/login', request);
    }
};

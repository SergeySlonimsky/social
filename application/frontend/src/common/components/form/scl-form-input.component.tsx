import React from 'react';
import Form from "react-bootstrap/Form";

interface IProps {
    name: string;
    type?: string;
    placeholder?: string;
    label?: string;
    value?: any;
    onChange: (e: React.ChangeEvent<any>) => void;
    onBlur: (e: React.FocusEvent<any>) => void;
}

export default class SclFormInput extends React.Component<IProps> {
    public render(): React.ReactNode {
        return (
            <Form.Group controlId={this.props.name}>
                {this.props.label && <Form.Label column={false}>{this.props.label}</Form.Label>}
                <Form.Control
                    name={this.props.name}
                    type={this.props.type || 'text'}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    onChange={this.props.onChange}
                    onBlur={this.props.onBlur}
                />
            </Form.Group>
        )
    }
}

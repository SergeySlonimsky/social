import React from 'react';
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

interface IProps {
    text?: string;
    isFormSending?: boolean;
}

export default class SclFormSubmit extends React.Component<IProps> {
    public render(): React.ReactNode {
        return (
            <Button
                type={'submit'}
                disabled={this.props.isFormSending}
            >
                {
                    this.props.isFormSending
                        ? <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/>
                        : (this.props.text || 'Submit')
                }
            </Button>
        );
    }
}

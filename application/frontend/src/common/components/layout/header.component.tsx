import React from 'react';
import {IUser} from "../../../user/models/user.model";
import {Link} from "react-router-dom";

interface IProps {
    profile: IUser| null;
    logout: () => void;
}

export default class Header extends React.Component<IProps> {
    public render(): React.ReactNode {
        return (
            <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                <Link to={'/'} className={'navbar-brand col-sm-3 col-md-2 mr-0'}>
                    <img src={'/img/logo.png'} alt={'logo'} height={30} /> Scl
                </Link>
                <ul className="navbar-nav px-3">
                    <li className="nav-item text-nowrap">
                        <button className="btn-link nav-link" onClick={this.props.logout}>Sign out ({this.props.profile ? this.props.profile.username : ''})</button>
                    </li>
                </ul>
            </nav>
        )
    }
}

import React from 'react';
import {Link} from "react-router-dom";

export default class Sidebar extends React.Component {
    public render(): React.ReactNode {
        return (
            <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                <div className="sidebar-sticky">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <Link to={'/'} className={'nav-link'}>Dashboard</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/'} className={'nav-link'}>Test</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/'} className={'nav-link'}>Test</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/'} className={'nav-link'}>Test</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

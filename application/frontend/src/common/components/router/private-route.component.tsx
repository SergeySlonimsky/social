import React from 'react';
import {Redirect, Route, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {IRootState} from "../../../root.state";

// @ts-ignore
const PrivateRoute = ({component: Component, isAuth, ...rest}) => {
    return <Route
        {...rest}
        render={
            props => {
                return isAuth
                    ? <Component {...props} />
                    : <Redirect to={{pathname: "/auth", state: {from: props.location}}} />
            }
        }
    />
};

const mapStateToProps = (state: IRootState) => ({isAuth: state.auth.isAuth});

// @ts-ignore
export default withRouter(connect(mapStateToProps)(PrivateRoute));

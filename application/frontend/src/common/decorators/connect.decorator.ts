import {connect} from 'react-redux';
import {IRootState} from "../../root.state";

export function Connect<TProps>(
    mapStateToProps: ((state: IRootState) => Partial<TProps>) | null,
    mapDispatchToProps?: any,
) {
    return (target: any) => (connect(mapStateToProps, mapDispatchToProps)(target) as any);
}

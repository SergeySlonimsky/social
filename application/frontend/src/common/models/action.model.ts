export type TBaseAction = {
    type: string;
};

export type TPayloadAction<TPayload> = TBaseAction & {
    payload: TPayload;
};

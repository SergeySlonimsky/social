export interface IResponse<T> {
    data: IResponseData<T>;
    status: number;
}

export interface IResponseData<T> {
    data: T;
}

export interface IResponseError {
    message: string;
    target: string;
    type: string;
    inner: IResponseError[];
}

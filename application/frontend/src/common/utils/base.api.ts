import axios, {AxiosPromise, AxiosRequestConfig} from "axios";
import {getToken} from "./tokenService";

const baseUrl = process.env.REACT_APP_DEV_API_URL;

export default class BaseApi {
    private static getRequestConfig = (): AxiosRequestConfig => {
        return {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        }
    }

    public static doGet = (url: string): AxiosPromise => {
        return axios.get(baseUrl + url, BaseApi.getRequestConfig());
    }

    public static doPost = (url: string, body: object | []): AxiosPromise => {
        return axios.post(baseUrl + url, JSON.stringify(body), BaseApi.getRequestConfig());
    }
}

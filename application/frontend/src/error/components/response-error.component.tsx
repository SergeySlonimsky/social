import React from 'react';
import {IError} from "../models/error.model";

interface IProps {
    error: IError;
}

const ResponseError: React.FunctionComponent<IProps> = (props: IProps) => {
    return (
        <>
            <p>{props.error.message}</p>
            <ul>
                {props.error.inner.map(item => <li>{item.message}</li>)}
            </ul>
        </>
    )
};

export default ResponseError;

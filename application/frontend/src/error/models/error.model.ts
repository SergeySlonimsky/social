export interface IError {
    type: string;
    target: string;
    message: string;
    inner: IError[];
}

export const generateNetworkError = () => {
    return {
        type: 'network_error',
        target: 'network',
        message: 'Network Error'
    }
}

import {
    generatePayloadActionCreator,
    TPayloadActionCreator
} from "../../common/state/generic.actions";
import {IError} from "../models/error.model";

export const setError: TPayloadActionCreator<IError|null> =
    generatePayloadActionCreator('error.setError');

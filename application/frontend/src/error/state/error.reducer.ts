import {combineReducers} from "redux";
import {generateObjectReducer} from "../../common/state/generic.reducers";
import {IError} from "../models/error.model";
import {setError} from "./error.action";
import {initialErrorState} from "./error.state";

export const errorReducer = combineReducers({
    error: generateObjectReducer<IError|null>(setError.type, initialErrorState.error),
});

export default errorReducer

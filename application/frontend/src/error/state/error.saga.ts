import {SagaIterator} from 'redux-saga';
import {put, call} from 'redux-saga/effects';
import {isLoading, logout} from "../../auth/store/auth.actions";
import {setError} from "./error.action";
import {setAppLoading} from "../../app/store/app.actions";

export function* errorHandler(err: any): SagaIterator {
    console.log(err);
    if (!err.response || err.response.status === 401) {
        yield put(logout());
    } else {
        yield put(isLoading(false));
        yield put(setAppLoading(false));
        yield put(setError(err.response.data));
    }
}

export const genericErrorHandler = (saga: (...args: any[]) => SagaIterator, ...args: any[]) =>
    function* handleApp(action: any): any {
        try {
            yield call(saga, action, args);
        } catch (err) {
            yield call(errorHandler, err);
        }
    };

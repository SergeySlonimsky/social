import {IError} from "../models/error.model";

export interface IErrorState {
    error: IError|null
}

export const initialErrorState: IErrorState = {
    error: null,
};

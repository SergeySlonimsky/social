import React from 'react';
import ReactDOM from 'react-dom';
import App from "./app/containers/app.container";
import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import configureStore, {history, sagaMiddleware} from './configureStore';
import {rootSaga} from "./root.saga";
import './styles/index.scss';

require('dotenv').config();

const store = configureStore();
sagaMiddleware.run(rootSaga as any);

ReactDOM.render(
    <Provider store={store}>
        <App history={history}/>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React from 'react';
import {Connect} from "../../../common/decorators/connect.decorator";
import {IUserState} from "../../../user/store/user.state";
import Header from "../../../common/components/layout/header.component";
import Footer from "../../../common/components/layout/footer.component";
import {logout} from "../../../auth/store/auth.actions";
import {getProfile} from "../../../user/store/user.actions";
import Sidebar from "../../../common/components/layout/sidebar.component";
import {IAppState} from "../../../app/store/app.state";

interface IProps {
    user: IUserState;
    app: IAppState;
    logout: () => void;
    getProfile: () => void;
}

@Connect(({user, app}) => ({
    app: app,
    user: user,
}), {
    logout: logout,
    getProfile: getProfile
})
export default class Home extends React.Component<IProps> {
    public componentDidMount(): void {
        this.props.getProfile();
    }

    public render(): React.ReactNode {
        return (
            <>
                <Header profile={this.props.user.profile} logout={this.props.logout}/>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar/>
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                            <div
                                className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                                <h1 className="h2">Dashboard</h1>
                            </div>
                        </main>
                    </div>
                </div>
                <Footer/>
            </>
        )
    }
}

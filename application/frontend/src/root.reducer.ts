import {combineReducers} from 'redux';
import {History} from 'history';
import {connectRouter} from 'connected-react-router';
import appReducer from "./app/store/app.reducer";
import authReducer from "./auth/store/auth.reducer";
import userReducer from "./user/store/user.reducer";
import errorReducer from "./error/state/error.reducer";

const rootReducer = (history: History) => combineReducers({
    app: appReducer,
    auth: authReducer,
    error: errorReducer,
    user: userReducer,
    router: connectRouter(history)
});

export default rootReducer

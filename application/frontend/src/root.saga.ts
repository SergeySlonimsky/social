import {SagaIterator} from 'redux-saga';
import {all, fork} from 'redux-saga/effects';
import {appSaga} from "./app/store/app.saga";
import {authSaga} from "./auth/store/auth.saga";
import {userSaga} from "./user/store/user.saga";

export function* rootSaga(): SagaIterator {
    yield all([
        fork(appSaga),
        fork(authSaga),
        fork(userSaga),
    ]) as any;
}

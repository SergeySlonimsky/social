import {RouterState} from "connected-react-router";
import {IAppState} from "./app/store/app.state";
import {IUserState} from "./user/store/user.state";
import {IAuthState} from "./auth/store/auth.state";
import {IErrorState} from "./error/state/error.state";

export interface IRootState {
    app: IAppState;
    auth: IAuthState;
    error: IErrorState;
    user: IUserState;
    router: RouterState;
}

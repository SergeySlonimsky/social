import {
    generateBaseActionCreator,
    generatePayloadActionCreator,
    TBaseActionCreator,
    TPayloadActionCreator,
} from '../../common/state/generic.actions';
import {IUser} from "../models/user.model";

export const getProfile: TBaseActionCreator =
    generateBaseActionCreator('user.getProfile');

export const setProfile: TPayloadActionCreator<IUser | null> =
    generatePayloadActionCreator('user.setProfile');

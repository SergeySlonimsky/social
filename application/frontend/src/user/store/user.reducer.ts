import {combineReducers} from "redux";
import {generateObjectReducer} from "../../common/state/generic.reducers";
import {setProfile} from "./user.actions";
import {IUser} from "../models/user.model";
import {initialUserState} from "./user.state";

export const userReducer = combineReducers({
    profile: generateObjectReducer<IUser|null>(setProfile.type, initialUserState.profile),
});

export default userReducer

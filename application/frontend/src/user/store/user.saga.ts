import {SagaIterator} from 'redux-saga';
import {call, put, takeLatest} from 'redux-saga/effects';
import {getProfile, setProfile} from "./user.actions";
import UserApi from "../utils/user.api";
import {genericErrorHandler} from "../../error/state/error.saga";

function* onGetProfile(): SagaIterator {
    const response = yield call(UserApi.getProfile);
    yield put(setProfile(response.data));
}

export function* userSaga(): SagaIterator {
    yield takeLatest(getProfile.type, genericErrorHandler(onGetProfile));
}

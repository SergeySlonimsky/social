import {IUser} from "../models/user.model";

export interface IUserState {
    profile: IUser | null;
}

export const initialUserState: IUserState = {
    profile: null,
}

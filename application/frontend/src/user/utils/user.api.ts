import BaseApi from "../../common/utils/base.api";

export default class UserApi {
    public static getProfile = () => {
        return BaseApi.doGet('api/user/profile');
    }
};

<?php

namespace App\Controller;

use App\Service\Api\ApiResponseInterface;
use App\Service\Api\ApiResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    /**
     * @var ApiResponseInterface
     */
    private $response;

    public function __construct(ApiResponseService $response)
    {
        $this->response = $response;
    }

    final protected function response($data = null, int $status = Response::HTTP_OK, array $headers = []): Response
    {
        $this->response->setHeaders($headers);

        return $this->response->response($data, $status);
    }
}

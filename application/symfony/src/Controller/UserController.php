<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route(path="/api/user")
 */
class UserController extends ApiController
{
    /**
     * @return Response
     *
     * @Route(
     *     "/profile",
     *     name="user_profile",
     *     methods={"GET"}
     * )
     */
    public function profile(): Response
    {
        $user = $this->getUser();

        return $this->response([
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
        ]);
    }
}

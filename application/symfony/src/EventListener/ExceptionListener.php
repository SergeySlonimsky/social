<?php

namespace App\EventListener;

use App\Service\Api\ApiResponseError;
use App\Service\Api\ApiResponseInterface;
use App\Service\Api\ApiResponseService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class ExceptionListener implements EventSubscriberInterface
{
    /**
     * @var ApiResponseInterface
     */
    private $response;

    public function __construct(ApiResponseService $response)
    {
        $this->response = $response;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
            ]
        ];
    }

    public function processException(GetResponseForExceptionEvent $event): void
    {
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $exception = $event->getException();

        if ($exception instanceof AuthenticationCredentialsNotFoundException) {
            $statusCode = Response::HTTP_UNAUTHORIZED;
        }

        $errorResponse = $this->response->response(
            new ApiResponseError(ApiResponseError::ERROR_TYPE_INTERNAL, '', $exception->getMessage()),
            $statusCode
        );

        $event->setResponse($errorResponse);
    }
}

<?php

namespace App\Service\Api;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiResponseService implements ApiResponseInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    private $headers = [
        'Content-Type' => 'application/json',
    ];

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function response($data = null, int $status = Response::HTTP_OK): Response
    {
        return new Response(
            $this->prepareData($data),
            $status,
            $this->headers
        );
    }

    public function setHeader(string $key, string $value): ApiResponseService
    {
        $this->headers[$key] = $value;

        return $this;
    }

    public function setHeaders(array $headers): ApiResponseService
    {
        foreach ($headers as $key => $value) {
            $this->setHeader($key, $value);
        }

        return $this;
    }

    private function prepareData($data): string
    {
        return $this->serializer->serialize($data, 'json');
    }
}

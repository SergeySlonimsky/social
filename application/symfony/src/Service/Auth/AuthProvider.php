<?php

namespace App\Service\Auth;

use App\Entity\User;
use App\Service\Api\Exception\ApiException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AuthProvider
 *
 * @package App\Service\Auth
 */
class AuthProvider
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    /**
     * AuthProvider constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param AuthenticationManagerInterface $authenticationManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder,
        AuthenticationManagerInterface $authenticationManager
    )
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @param array $data
     * @return int
     * @throws ApiException
     */
    public function register(array $data): int
    {
        try {
            $user = new User();
            $user
                ->setUsername($data['username'])
                ->setEmail($data['email'])
                ->setPassword($this->encoder->encodePassword($user, $data['password']));
            $this->em->persist($user);
            $this->em->flush();

            return $user->getId();
        } catch (ORMException $exception) {
            throw new ApiException('Username or email already exists', Response::HTTP_BAD_REQUEST, $exception);
        } catch (\Exception $exception) {
            throw new ApiException('Internal error', Response::HTTP_INTERNAL_SERVER_ERROR, $exception);
        }
    }
}
